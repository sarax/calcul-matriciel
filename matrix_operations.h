#ifndef MATRIX_OPERATIONS_H
#define MATRIX_OPERATIONS_H

// Fonction pour allouer de la mémoire pour une matrice de taille n x n
float **allocateMatrix(int n);

// Fonction pour libérer la mémoire allouée pour une matrice
void freeMatrix(float **matrix, int n);

// Fonction pour lire une matrice depuis un fichier
void lireMatrice(FILE *file, int n, float **matrix);

// Fonction pour imprimer une matrice
void printMatrix(int n, float **matrix);

// Fonction pour multiplier deux matrices de taille n x n
void matrixMultiplication(int n, float **mat1, float **mat2, float **result);

// Fonction pour inverser une matrice de taille n x n
void inverseAlgorithm(int n, float **matrix);

// Fonction pour soustraire deux matrices
void matrixSum(int n, float **mat1, float **mat2, float **result);

// Fonction pour additionner deux matrices
void matrixSumplus(int n, float **mat1, float **mat2, float **result);



#endif /* MATRIX_OPERATIONS_H */
