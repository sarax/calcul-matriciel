#include <stdio.h>
#include <stdlib.h>
#include "matrix_operations.h"

// Fonction pour allouer de la mémoire pour une matrice de taille n x n
float **allocateMatrix(int n) {
    float **matrix = (float **)malloc(n * sizeof(float *));
    for (int i = 0; i < n; i++) {
        matrix[i] = (float *)malloc(n * sizeof(float));
    }
    return matrix;
}

// Fonction pour libérer la mémoire allouée pour une matrice
void freeMatrix(float **matrix, int n) {
    for (int i = 0; i < n; i++) {
        free(matrix[i]);
    }
    free(matrix);
}

// Fonction pour lire une matrice depuis un fichier
void lireMatrice(FILE *file, int n, float **matrix) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            fscanf(file, "%f", &matrix[i][j]);
        }
    }
}

// Fonction pour afficher une matrice
void printMatrix(int n, float **matrix) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%.3f\t", matrix[i][j]);
        }
        printf("\n");
    }
}

// Fonction pour multiplier deux matrices de taille n x n
void matrixMultiplication(int n, float **mat1, float **mat2, float **result) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            result[i][j] = 0;
            
            for (int k = 0; k < n; k++) {
                
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }
}
/*
int main() {
    FILE *file1 = fopen("matrix1.txt", "r"); // Fichier contenant la première matrice
    FILE *file2 = fopen("matrix2.txt", "r"); // Fichier contenant la deuxième matrice

    int n; // Taille de la matrice carrée
    fscanf(file1, "%d", &n);

    // Allocation de mémoire pour les matrices
    float **mat1 = allocateMatrix(n);
    float **mat2 = allocateMatrix(n);
    float **result = allocateMatrix(n);

    lireMatrice(file1, n, mat1);
    lireMatrice(file2, n, mat2);
    printf("Matrice 1:\n");
    printMatrix(n, mat1);
    printf("Matrice 2:\n");
    printMatrix(n, mat2);

    
    fclose(file1);
    fclose(file2);

    matrixMultiplication(n, mat1, mat2, result);

    // Affichage du résultat
    printf("Result  du produit des deux matrices:\n");
    printMatrix(n, result);

    // Libération de la mémoire allouée pour les matrices
    freeMatrix(mat1, n);
    freeMatrix(mat2, n);
    freeMatrix(result, n);

    return 0;
}
*/