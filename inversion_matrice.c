#include <stdio.h>
#include <stdlib.h>
#include "matrix_operations.h"
//Fonction pour soustraire  deux matrices
void matrixSum(int n, float **mat1, float **mat2, float **result) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            result[i][j] = mat1[i][j] - mat2[i][j];
        }
    }
}
//Fonction pour additionner  deux matrices
void matrixSumplus(int n, float **mat1, float **mat2, float **result) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            result[i][j] = mat1[i][j] + mat2[i][j];
        }
    }
}
//Fonction pour multiplier  une matrice et un scalaire
void matrixMult(int n, float **mat, float f) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            mat[i][j] *= f;
        }
    }
}
// Fonction pour calculer la transposée d'une matrice carrée
void transposeMatrix(int n, float **matrix) {
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            float temp = matrix[i][j];
            matrix[i][j] = matrix[j][i];
            matrix[j][i] = temp;
        }
    }
}

// Fonction pour inverser une matrice de taille n x n
void inverseAlgorithme(int n, float **matrix) {
    if (n == 1) {
        if (matrix[0][0] == 0) {
            printf("La matrice n'est pas inversible.\n");
            return;
        }
        matrix[0][0] = 1.0 / matrix[0][0];
        return;
    }
    
    
    float **matrixcopie = allocateMatrix(n);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            matrixcopie[i][j] = matrix[i][j];
        }
    }
    
    transposeMatrix(n, matrixcopie);
          
    
    float **result = allocateMatrix(n);
    matrixMultiplication(n, matrixcopie, matrix, result);
   
    int newSize = n / 2;
    
    float **B = allocateMatrix(newSize);
    float **C = allocateMatrix(newSize);
    float **D = allocateMatrix(newSize);
    
    

    for (int i = 0; i < newSize; i++) {
        for (int j = 0; j < newSize; j++) {
            B[i][j] = result[i][j];
            C[i][j] = result[i][j + newSize];
            D[i][j] = result[i + newSize][j + newSize];
        }
    }
    
     
    float **Binv = allocateMatrix(newSize);
    
    for (int i = 0; i < newSize; i++) {
        for (int j = 0; j < newSize; j++) {
            Binv[i][j] = B[i][j];
        }
    }
    inverseAlgorithme(newSize,Binv);
    
 
    //
    float **TransC = allocateMatrix(newSize);
    for (int i = 0; i < newSize; i++) {
        for (int j = 0; j < newSize; j++) {
            TransC[i][j] = C[i][j];
        }
    }
    transposeMatrix(newSize,TransC);
    
    //
    float **CBinv = allocateMatrix(newSize);
    matrixMultiplication(newSize, C, Binv,CBinv);
    //
    float **TransCBinv = allocateMatrix(newSize);
    for (int i = 0; i < newSize; i++) {
        for (int j = 0; j < newSize; j++) {
            TransCBinv[i][j] = CBinv[i][j];
        }
    }
    transposeMatrix(newSize,TransCBinv);
    
    // Calcul de S=D-D-CBinv TransC
    float **VraiS = allocateMatrix(newSize);
    matrixMultiplication(newSize,CBinv,TransC,VraiS);
    
    float **S = allocateMatrix(newSize);
    matrixSum(newSize, D, VraiS,S) ;
   // S

    float **Sinv = allocateMatrix(newSize);
    for (int i = 0; i < newSize; i++) {
        for (int j = 0; j < newSize; j++) {
            Sinv[i][j] = S[i][j];
        }
    }
    inverseAlgorithme(newSize,Sinv);
    //

    float **SinvCBinv = allocateMatrix(newSize);
    matrixMultiplication(newSize,Sinv,CBinv,SinvCBinv);
    //

    float **TransSinvCBinv = allocateMatrix(newSize);
    for (int i = 0; i < newSize; i++) {
        for (int j = 0; j < newSize; j++) {
            TransSinvCBinv[i][j] = SinvCBinv[i][j];
        }
    }
    transposeMatrix(newSize,TransSinvCBinv);
    //

    float **last = allocateMatrix(newSize);
    matrixMultiplication(newSize,TransCBinv,SinvCBinv,last);
    //

    float **lastplus = allocateMatrix(newSize);
    matrixSumplus(newSize, Binv, last, lastplus);

    matrixMult(newSize, TransSinvCBinv,-1);
    matrixMult(newSize, SinvCBinv,-1);
    // 

    float **Final = allocateMatrix(n);
    for (int i = 0; i < newSize; i++) {
        for (int j = 0; j < newSize; j++) {
            Final[i][j] = lastplus[i][j]; 
            Final[i][j + newSize] = TransSinvCBinv[i][j]; 
            Final[i + newSize][j] = SinvCBinv[i][j]; 
            Final[i + newSize][j + newSize] = Sinv[i][j];
            
        }
    }

  
    float **Bineta = allocateMatrix(n);
    matrixMultiplication(n,Final,matrixcopie,Bineta);
    

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            matrix[i][j] = Bineta[i][j];
        }
    }

    
    freeMatrix(B, newSize);
    freeMatrix(C, newSize);
    freeMatrix(D, newSize);
    freeMatrix(S, newSize);
    freeMatrix(VraiS, newSize);
    freeMatrix(Bineta,n);
    freeMatrix(last,newSize);
    freeMatrix(lastplus,newSize);
    freeMatrix(TransCBinv,newSize);
    freeMatrix(SinvCBinv,newSize);
    freeMatrix(Sinv,newSize);
    freeMatrix(Final,n);
    freeMatrix(matrixcopie, n);
    freeMatrix(result, n);
}


/*
int main() {
    FILE *file = fopen("matrix1.txt", "r");
    if (file == NULL) {
        printf("Erreur lors de l'ouverture du fichier.\n");
        return 1;
    }

    int n; 
    fscanf(file, "%d", &n);

    float **matrix = allocateMatrix(n);

    lireMatrice(file, n, matrix);

    fclose(file);

    printf("Matrice originale :\n");
    printMatrix(n, matrix);

    inverseAlgorithme(n, matrix);
    
    printf("\nInverse de la matrice :\n");
    printMatrix(n, matrix);    

    freeMatrix(matrix, n);

    return 0;
}*/